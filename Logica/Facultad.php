<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/FacultadDAO.php";
class Producto{
    private $idFacultad;
    private $nombre;
    private $direccion;
    private $telefono;
    private $conexion;
    private $facultadDAO;
    
    public function getIdFacultad(){
        return $this -> idFacultad;
    }
    
    public function getNombre(){
        return $this -> nombre;
    }
    
    public function getDireccion(){
        return $this -> direccion;
    }
    
    public function getTelefono(){
        return $this -> telefono;
    }
        
    public function Producto($idFacultad = "", $nombre = "", $direccion = "", $telefono = ""){
        $this -> idProducto = $idFacultad;
        $this -> nombre = $nombre;
        $this -> cantidad = $direccion;
        $this -> precio = $telefono;
        $this -> conexion = new Conexion();
        $this -> productoDAO = new FacultadDAO($this -> idFacultad, $this -> nombre, $this -> direccion, $this -> telefono);
    }
    
    public function insertar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> FacultadDAO -> insertar());        
        $this -> conexion -> cerrar();        
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this ->FacultadDAO -> consultarTodos());
        $facultades = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $f = new Producto($resultado[0], $resultado[1], $resultado[2], $resultado[3]);
            array_push($facultades, $f);
        }
        $this -> conexion -> cerrar();        
        return $facultades;
    }
    
    public function consultarPaginacion($cantidad, $pagina){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> FacultadDAO -> consultarPaginacion($cantidad, $pagina));
        $facultades = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $f = new Facultad($resultado[0], $resultado[1], $resultado[2], $resultado[3]);
            array_push($faculdades, $f);
        }
        $this -> conexion -> cerrar();
        return $facultades;
    }
    
}

?>