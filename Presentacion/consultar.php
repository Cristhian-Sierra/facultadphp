<?php
$facultad = new Facultad();
$cantidad = 5;
if(isset($_GET["cantidad"])){
    $cantidad = $_GET["cantidad"];
}
$pagina = 1;
if(isset($_GET["pagina"])){
    $pagina = $_GET["pagina"];
}
$facultades = $facultad -> consultarPaginacion($cantidad, $pagina);
?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-white bg-info">
					<h4>Consultar Facultad</h4>
				</div>
				<div class="text-right"><?php echo count($facultades) ?> registros encontrados</div>
              	<div class="card-body">
					<table class="table table-hover table-striped">
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>Direccion</th>
							<th>Telefono</th>
						</tr>
						<?php 
						$i=1;
						foreach($facultades as $facultadActual){
						    echo "<tr>";
						    echo "<td>" . $i . "</td>";
						    echo "<td>" . $facultadActual -> getNombre() . "</td>";
						    echo "<td>" . $facultadActual -> getDireccion() . "</td>";
						    echo "<td>" . $facultadActual -> getTelefono() . "</td>";
						    echo "</tr>";
						    $i++;
						}
						?>
					</table>
					<div class="text-center">
        				<nav>
        					<ul class="pagination">
        						<li class="page-item disabled"><span class="page-link"> &lt;&lt; </span>
        						</li>
        						<li class="page-item"><a class="page-link" href="index.php?pid=<?php echo base64_encode("presentacion/inicio(crear).php") ?>&pagina=1">1</a></li>
        						<li class="page-item active" aria-current="page"><span
        							class="page-link"> 2 <span class="sr-only">(current)</span>
        						</span></li>
        						<li class="page-item"><a class="page-link" href="#">3</a></li>
        						<li class="page-item"><a class="page-link" href="#"> &gt;&gt; </a></li>
        					</ul>
        				</nav>
					</div>
				</div>
            </div>
		</div>
	</div>
</div>